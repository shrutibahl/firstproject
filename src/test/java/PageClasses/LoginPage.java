package PageClasses;

import org.openqa.selenium.By;

import BaseClasses.BaseClass;

public class LoginPage extends BaseClass {

	By uname = By.id("uid");
	By pwd = By.id("passw");
	By submit = By.name("btnSubmit");
	By signOff = By.xpath("//font[text()='Sign Off']");
	
	By lstAccount = By.id("listAccounts");

	public void login(String username, String passwod) {
		sendKey(uname, username);
		sendKey(pwd, passwod);
		click(submit);
	}
	
	public boolean verifyLogin() {

		return isDisplayed(signOff);
	}
	
	
	
}
