package BaseClasses;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import Utilities.ActionUtility;
import Utilities.GenricFunctions;

public class BaseClass {

	public static WebDriver driver;
	public static Properties prop;
	public static ActionUtility act;
	public static GenricFunctions gf;

	public BaseClass() {
		try {
			File f = new File(".//Configuration.properties");
			FileInputStream fis = new FileInputStream(f);

			prop = new Properties();
			prop.load(fis);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void intialize() {
		String path = System.getProperty("user.dir") + "\\Resources\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", path);
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(4000, TimeUnit.SECONDS);
		driver.get(prop.getProperty("url"));
		driver.manage().window().maximize();
		act = new ActionUtility(driver);
		gf = new GenricFunctions(driver);
	}

	public WebElement getElement(By locator) {
		WebElement we = driver.findElement(locator);
		return we;
	}

	public void sendKey(By locator, String keysToSend) {
		getElement(locator).sendKeys(keysToSend);
	}

	public void click(By locator) {
		getElement(locator).click();
	}

	public boolean isDisplayed(By locator) {
		try {
			getElement(locator).isDisplayed();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
