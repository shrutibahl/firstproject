package Utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class ActionUtility {
	
	public WebDriver driver;
	
	public ActionUtility(WebDriver driver) {
		this.driver = driver;		
	}

	public void hoverOnElement(WebElement we) {
		Actions ac = new Actions(driver);
		ac.moveToElement(we).build().perform();		
	}
	
	
}
