package Utilities;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class GenricFunctions {

	
public WebDriver driver;
	
	public GenricFunctions(WebDriver driver) {
		this.driver = driver;
		
	}
	
	public void switchToAlert() {
		driver.switchTo().alert();
	}

	public void acceptAlert() {
		Alert at =  driver.switchTo().alert();
		at.accept();
	}
	public void dismissAlert() {
		Alert at =  driver.switchTo().alert();
		at.dismiss();
	}
	public String getAlertText() {
		Alert at =  driver.switchTo().alert();
		return at.getText();
	}
  public void selectValueByIndex(WebElement we,int index) {
	  Select sel = new Select(we);
	  sel.selectByIndex(index);
  }

}
