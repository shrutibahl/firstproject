package TestClasses;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseClasses.BaseClass;
import PageClasses.LoginPage;

public class LoginTest extends BaseClass{
 
	
	LoginPage lp ;
	
  @BeforeMethod
  public void beforeMethod() {	  
	  intialize();
	  lp = new LoginPage();
  }

  
  @Test(description = "Verify that user is able to login successfully with valid credentals")
  public void validLogin() {
	 String  username =prop.getProperty("uname");
	 String  password =prop.getProperty("pass");
	lp.login(username, password);  
	Assert.assertTrue(lp.verifyLogin(), "Test case failed : As user is not able to login");
	
  }
  
  @Test(description = "Verify that user is able to login successfully with valid credentals")
  public void invalidLogin() {
	 String  username =prop.getProperty("uname");
	 String  password =prop.getProperty("pass");
	lp.login(username + "asdfas", password);  
	Assert.assertFalse(lp.verifyLogin(), "Test case failed : As user is  able to login");
	
  }
  
  
  
  
  
  
  @AfterMethod
  public void afterMethod() {
	  driver.quit();
  }

}
